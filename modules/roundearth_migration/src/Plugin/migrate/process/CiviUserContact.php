<?php

namespace Drupal\roundearth_migration\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\MigrateSkipProcessException;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\roundearth_migration\CiviCRM\CiviCrmAwareTrait;

/**
 * Gets the contact ID from a user UID.
 *
 * @MigrateProcessPlugin(
 *   id = "roundearth_migration_civi_user_contact"
 * )
 */
class CiviUserContact extends ProcessPluginBase {

  use CiviCrmAwareTrait;

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $this->getCivi()->initialize();
    $result = civicrm_api3('UFMatch', 'get', ['uf_id' => $value]);

    // If not found.
    if (empty($result['values'])) {
      throw new MigrateSkipProcessException();
    }

    $value = reset($result['values']);
    return $value['contact_id'];
  }

}
