<?php

namespace Drupal\roundearth_migration\Plugin\migrate\process;

use Drupal\roundearth_migration\ConfigAwareTrait;

/**
 * Class LinkMap.
 *
 * Converts files and images found within text to media entities and embeds them
 * within the WYSIWYG text.
 *
 * @MigrateProcessPlugin(
 *   id = "roundearth_migration_link_map"
 * )
 */
class LinkMap extends UrlProcessorBase {

  use ConfigAwareTrait;

  protected $defaultConfiguration = [
    'hosts' => NULL,
    'link_map' => [],
  ];

  /**
   * {@inheritdoc}
   */
  protected function processUrl($url) {
    if (!$url = $this->internalUrl($url)) {
      return FALSE;
    }

    $newUrl = $url;
    foreach ($this->getConfiguration()['link_map'] as $src => $dest) {
      $src = trim($src);
      $src = '/' . trim($src, '/');
      $dest = trim($dest);
      $dest = '/' . trim($dest, '/');
      if ($mapped = $this->mapUrl($src, $dest, $newUrl)) {
        $newUrl = $mapped;
      }
    }

    return $url != $newUrl ? $newUrl : NULL;
  }

  /**
   * Maps a URL.
   *
   * @param string $src
   *   Map source.
   * @param string $dest
   *   Map destination.
   * @param string $url
   *
   * @return string
   *   The mapped URL.
   */
  protected function mapUrl($src, $dest, $url) {
    // Direct match.
    if ($src == $url) {
      return $dest;
    }

    // Matches parts.
    if (strpos($url, $src . '/') === 0) {
      $suffix = substr($url, strlen($src));
      return $dest . $suffix;
    }

    return FALSE;
  }

}
