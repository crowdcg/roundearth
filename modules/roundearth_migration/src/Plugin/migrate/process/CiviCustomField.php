<?php

namespace Drupal\roundearth_migration\Plugin\migrate\process;

use Drupal\migrate\MigrateSkipProcessException;
use Drupal\roundearth_migration\CiviCRM\CiviCrmAwareTrait;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Class CiviCustomField.
 *
 * @MigrateProcessPlugin(
 *   id = "roundearth_migration_civicrm_custom_field"
 * )
 */
class CiviCustomField extends ProcessPluginBase {

  use CiviCrmAwareTrait;

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $this->getCivi()->initialize();
    if (!$field = $this->getCustomFields()->getField($destination_property)) {
      throw new MigrateSkipProcessException();
    }

    if (!empty($field['option_group_id'])) {
      $value = $this->getCustomFields()->getSelectValue($field, $value);
    }

    $row->setDestinationProperty($this->getDestinationProperty($field), $value);
  }

  protected function getDestinationProperty(array $field) {
    return 'custom_' . $field['id'];
  }

  /**
   * @return \Drupal\roundearth_migration\CiviCRM\CustomFields
   */
  protected function getCustomFields() {
    return \Drupal::service('roundearth_migration.civicrm_custom_fields');
  }

}
