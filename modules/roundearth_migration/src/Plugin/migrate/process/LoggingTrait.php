<?php

namespace Drupal\roundearth_migration\Plugin\migrate\process;

use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Trait LoggingTrait.
 */
trait LoggingTrait {

  /**
   * The migration, or NULL if not available.
   *
   * @var \Drupal\migrate\MigrateExecutableInterface|null
   */
  protected $migrateExecutable;

  /**
   * Logs a message on the current migration.
   *
   * @param string $message
   *   The message.
   * @param int $level
   *   The message level, defaults to ERROR.
   */
  protected function log($message, $level = MigrationInterface::MESSAGE_ERROR) {
    if ($this->migrateExecutable) {
      $this->migrateExecutable->saveMessage($message, $level);
    }
  }

}
