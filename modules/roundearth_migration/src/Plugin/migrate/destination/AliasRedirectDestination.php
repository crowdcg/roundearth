<?php

namespace Drupal\roundearth_migration\Plugin\migrate\destination;

use Drupal\migrate\MigrateException;
use Drupal\migrate\Plugin\migrate\destination\DestinationBase;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Drupal\redirect\Entity\Redirect;

/**
 * Class AliasRedirectDestination.
 *
 * @MigrateDestination(
 *   id = "roundearth_migration_alias_redirect"
 * )
 */
class AliasRedirectDestination extends DestinationBase {

  /**
   * {@inheritdoc}
   */
  public function getIds() {
   return [
     'id' => [
       'type' => 'integer',
       'unsigned' => FALSE,
       'size' => 'big',
     ],
     'type' => [
       'type' => 'string',
       'max_length' => 64,
       'is_ascii' => TRUE,
     ],
   ];
  }

  /**
   * {@inheritdoc}
   */
  public function fields(MigrationInterface $migration = NULL) {
    return [
      'source' => $this->t('Source'),
      'alias' => $this->t('Alias'),
      'langcode' => $this->t('Language code'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function import(Row $row, array $old_destination_id_values = []) {
    $source = $row->getDestinationProperty('source');
    $alias = $row->getDestinationProperty('alias');
    $langcode = $row->getDestinationProperty('langcode');

    // If no existing alias, make one.
    if (!$existingAlias = $this->getAlias($source, $langcode)) {
      $result = $this->getAliasStorage()->save($source, $alias, $langcode);
      return [
        'id' => $result['pid'],
        'type' => 'alias',
      ];
    }

    // If alias matches existing, nothing left to do.
    if ($alias == $existingAlias) {
      throw new MigrateException(sprintf('Alias exists for source "%s" and destination "%s", langcode "%s".', $source, $alias, $langcode));
    }

    // Needs redirect.
    $redirect = Redirect::create([
      'status_code' => 301,
      'language' => $langcode,
      'redirect_source' => ltrim($alias, '/'),
      'redirect_redirect' => 'internal:' . $source,
    ]);
    $redirect->save();

    return [
      'id' => $redirect->id(),
      'type' => 'redirect',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function supportsRollback() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function rollback(array $destination_identifier) {
    switch ($destination_identifier['type']) {
      case 'alias':
        $this->getAliasStorage()->delete([
          'pid' => $destination_identifier['id'],
        ]);
        break;

      case 'redirect':
        if ($redirect = $this->getRedirectStorage()->load($destination_identifier['id'])) {
          $this->getRedirectStorage()->delete([$redirect]);
        }
        break;
    }
  }

  /**
   * Gets an existing alias.
   *
   * @param string $source
   *   The alias source.
   * @param string $langcode
   *   The alias language code.
   *
   * @return string|false
   *   The alias string, or FALSE if not found.
   */
  protected function getAlias($source, $langcode) {
    return $this->getAliasStorage()->lookupPathAlias($source, $langcode);
  }

  /**
   * Gets the alias storage service.
   *
   * @return \Drupal\Core\Path\AliasStorageInterface
   *   The alias storage service.
   */
  protected function getAliasStorage() {
    return \Drupal::service('path.alias_storage');
  }

  /**
   * Gets the redirect storage service.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *   The redirect storage service.
   */
  protected function getRedirectStorage() {
    return \Drupal::entityTypeManager()->getStorage('redirect');
  }

}
