<?php

/**
 * @file
 * Round Earth Access module code.
 */

use Drupal\Core\Session\AccountInterface;
use Drupal\node\NodeInterface;

/**
 * Implements hook_node_access_records().
 */
function roundearth_access_node_access_records(NodeInterface $node) {
  $grants = [];

  if ($node->isPublished()) {
    return $grants;
  }

  $grants[] = [
    'realm' => 'roundearth_access_view_any_unpublished',
    'gid' => 1,
    'grant_view' => 1,
    'grant_update' => 0,
    'grant_delete' => 0,
  ];

  $grants[] = [
    'realm' => 'roundearth_access_view_own_unpublished',
    'gid' => $node->getOwnerId(),
    'grant_view' => 1,
    'grant_update' => 0,
    'grant_delete' => 0,
  ];

  return $grants;
}

/**
 * Implements hook_node_grants().
 */
function roundearth_access_node_grants(AccountInterface $account, $op) {
  $grants = [];

  // Permission to view any unpublished content.
  if ($account->hasPermission('view any unpublished content')) {
    $grants['roundearth_access_view_any_unpublished'] = [1];
  }

  // Permission to view own unpublished content.
  if ($account->hasPermission('view own unpublished content')) {
    $grants['roundearth_access_view_own_unpublished'] = [$account->id()];
  }

  return $grants;
}
