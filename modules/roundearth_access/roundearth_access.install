<?php

/**
 * @file
 * Install hooks for roundearth_access module.
 */

use \Drupal\taxonomy\Entity\Term;

/**
 * Implements hook_install().
 */
function roundearth_access_install() {
  /** @var \Drupal\Core\Config\ConfigFactoryInterface $config_factory */
  $config_factory = \Drupal::service('config.factory');
  $tac_lite_config = $config_factory->getEditable('tac_lite.settings');

  // Only override if the configuration hasn't been changed.
  if (!empty($tac_lite_config->get('tac_lite_categories'))) {
    return;
  }

  $data = [
    'tac_lite_categories' => [
      'roundearth_access' => 'roundearth_access',
    ],
    'tac_lite_schemes' => '2',
    'menu_rebuild_needed' => FALSE,
    'tac_lite_config_scheme_1' => [
      'name' => 'Read',
      'perms' => [
        'grant_view' => 'grant_view',
      ],
      'unpublished' => 0,
      'term_visibility' => 0,
    ],
    'tac_lite_grants_scheme_1' => [],
    'tac_lite_config_scheme_2' => [
      'name' => 'Read and write',
      'perms' => [
        'grant_view' => 'grant_view',
        'grant_update' => 'grant_update',
      ],
      'unpublished' => 0,
      'term_visibility' => 0,
    ],
    'tac_lite_grants_scheme_2' => [],
  ];

  $term_definitions = [
    'Public' => 'c47b5ac4-062d-4fd3-9465-5d757f9a9d2c',
    'Members only' => '5c2a1167-a0c1-4ba3-aa72-3bd4ea74d9a8',
  ];
  $term_objects = _roundearth_access_lookup_or_create_terms($term_definitions);

  $schemes = [
    '1' => [
      'anonymous' => ['Public'],
      'authenticated' => ['Public'],
      'member' => ['Public', 'Members only'],
      'editor' => ['Public', 'Members only'],
      'manager' => ['Public', 'Members only'],
      'administrator' => ['Public', 'Members only'],
    ],
    '2' => [
      'anonymous' => [FALSE],
      'authenticated' => [FALSE],
      'member' => [FALSE],
      'editor' => ['Public', 'Members only'],
      'manager' => ['Public', 'Members only'],
      'administrator' => ['Public', 'Members only'],
    ],
  ];

  // Put the grants into the config the way tac_lite expects them.
  foreach ($schemes as $scheme => $roles) {
    foreach ($roles as $role => $term_labels) {
      foreach ($term_labels as $term_label) {
        $tid = $term_label ? $term_objects[$term_label]->id() : '0';
        $data["tac_lite_grants_scheme_{$scheme}"][$role]['roundearth_access'][$tid] = $tid;
      }
    }
  }

  $tac_lite_config->merge($data);
  $tac_lite_config->save();
}

/**
 * Lookup or create terms.
 *
 * @param array $term_definitions
 * @return \Drupal\taxonomy\Entity\Term[]
 */
function _roundearth_access_lookup_or_create_terms(array $term_definitions) {
  /** @var \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository */
  $entity_repository = \Drupal::service('entity.repository');

  $term_objects = [];
  $weight = 0 - count($term_definitions);
  foreach ($term_definitions as $label => $uuid) {
    $term = $entity_repository->loadEntityByUuid('taxonomy_term', $uuid);
    if (!$term) {
      $term = Term::create([
        'vid' => 'roundearth_access',
        'name' => $label,
        'uuid' => $uuid,
        'weight' => $weight,
      ]);
      $term->save();
      $weight++;
    }
    $term_objects[$label] = $term;
  }

  return $term_objects;
}

