<?php
/**
 * @file
 * Theme and preprocess functions for blocks.
 */

use Drupal\block\Entity\Block;

function pangea_theme_suggestions_block_alter(array &$suggestions, array $variables) {
  $element = $variables['elements'];

  // Add a template suggestion based on the block's region.
  if (!empty($element['#id'])) {
    $block = Block::load($element['#id']);
    $suggestions[] = 'block__' . $block->getRegion();
    $suggestions[] = 'block__' . $block->bundle() . '__' . $block->getRegion();
  }

  return $suggestions;
}
